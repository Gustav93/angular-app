import { Component, OnInit } from '@angular/core';
import {User} from '../User';
import {UserService} from '../service/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert2';
import * as bcrypt from 'bcryptjs';
import { Role } from '../Role';

@Component({
  selector: 'app-form',
  templateUrl: './user.form.component.html'
})

export class UserFormComponent implements OnInit {

  title = 'Create User';

  user: User = new User();

  roles: Role[] = [];

  constructor(private userService: UserService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.userService.getRoles().subscribe( roles => {console.log(roles);this.roles = roles;});
    this.loadUser();
  }

  private loadUser(): void {
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.userService.getClient(id).subscribe(user => {console.log(user); this.user = user;});
      }
    });
  }

  createUser(): void {
    this.user.roles = [{id: 2 , name: 'ROLE_USER'}];
    this.user.enabled = true;
    this.user.password = bcrypt.hashSync(this.user.password, bcrypt.genSaltSync(10));
    this.userService.create(this.user).subscribe(user => {
      this.router.navigate(['/users']);
      swal.fire('Nuevo User', `User ${this.user.name} ${this.user.surname} successfully created`, 'success');
    });
  }

  updateUser(): void {
    this.userService.getClient(this.user.id).subscribe(userSource => {
      if (userSource.password !== this.user.password) {
        this.user.password = bcrypt.hashSync(this.user.password, bcrypt.genSaltSync(10));
      }
      this.userService.update(this.user).subscribe(user => {
        this.router.navigate(['/users']);
        swal.fire('User Updated', `User ${user.name} ${user.surname} successfully updated`, 'success');
      });
    });
  }

  compareRole(o1: Role, o2: Role): boolean {
    return (o1 == null || o2 == null) ? false : o1.id === o2.id;
  }
}
