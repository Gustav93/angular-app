import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html'
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() paginator: any;

  paginas: number[];

  start: number;
  end: number;

  constructor() { }

  ngOnInit() {
    this.calculateRange();
  }

  ngOnChanges(changes: SimpleChanges) {
    const paginatorRefreshed = changes.paginator;

    if ( paginatorRefreshed.previousValue) {
      this.calculateRange();
    }
  }

  calculateRange(): void {
    this.start = Math.min(Math.max(this.paginator.number, this.paginator.number - 2), this.paginator.totalPages - 3);
    this.end = Math.max(Math.min(this.paginator.totalPages, this.paginator.number + 2), 3);
    console.log(this.start + '-' + this.end);
    if(this.start>0 ) {
      if (this.paginator.totalPages > 3) {
        this.paginas = new Array(this.end - this.start + 1).fill(0)
        .map((value, index) => index + this.start);
      } else {
        this.paginas = new Array(this.paginator.totalPages).fill(0)
        .map((value, index) => index + 1);
        console.log('asd');
      }
    }
    
    console.log(this.paginas);
  }
}
